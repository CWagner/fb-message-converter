﻿using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FB_Message_Parser
{
    class Program
    {
        static void Main(string[] args)
        {
            var doc = new HtmlDocument();
            if (args.Count() < 2)
                throw new Exception();
            doc.Load(args[0], Encoding.UTF8);
            var convos = new List<Conversation>();
            foreach (var thread in doc.DocumentNode.SelectNodes("//div[@class='thread']"))
            {
                var convo = new Conversation();
                convo.Members = thread.FirstChild.InnerText.Split(new[] { ", " }, StringSplitOptions.RemoveEmptyEntries).ToList();
                var headers = thread.SelectNodes(".//div[@class='message_header']");
                var messages = thread.SelectNodes(".//p");
                for (int idx = 0; idx < headers.Count; idx++)
                {
                    var msg = new Message();
                    var header = headers[idx];
                    var message = messages[idx].InnerText;
                    msg.From = header.SelectSingleNode(".//span[@class='user']").InnerText;
                    var unparsedDate = header.SelectSingleNode(".//span[@class='meta']").InnerText;
                    msg.Date = DateTime.ParseExact(unparsedDate, "dddd, d MMMM yyyy 'at' HH:mm 'UTC'zz", null);
                    msg.Text = HtmlEntity.DeEntitize(message);
                    convo.Messages.Add(msg);
                }
                convo.Messages = convo.Messages.OrderBy(m => m.Date).ToList();
                convos.Add(convo);
            }
            File.WriteAllText(args[1], JsonConvert.SerializeObject(convos), Encoding.UTF8);
        }
    }
    public class Conversation
    {
        public Conversation()
        {
            Messages = new List<Message>();
        }
        public List<string> Members { get; set; }
        public List<Message> Messages { get; set; }
    }

    public class Person
    {
        public string Name { get; set; }
    }

    public class Message
    {
        public DateTime Date { get; set; }
        public string From { get; set; }
        public string Text { get; set; }
    }
}
